import UIKit

public protocol UndoItem {}
public protocol UndoDelegate {
    func undoItem() -> Any?
    func apply(undoItem: Any)
}

public class Undo<A> {
    public var undoAvailable: Bool {undoStack.isEmpty == false}
    public var redoAvailable: Bool {redoStack.isEmpty == false}
    public var undoCount: Int {return undoStack.count}
    public var redoCount: Int {return redoStack.count}

    public init(delegate: UndoDelegate) {
        self.delegate = delegate
    }
            
    public func clear() {
        undoStack.clear()
        redoStack.clear()
    }
    
    public func save() {
        push()
        redoStack.clear()
    }
    public func undo() {
        pop()
    }
    public func redo() {
        guard let redoItem = redoStack.pop() else {return}
        push()
        delegate.apply(undoItem: redoItem)
    }

    private let undoStack = UndoStack<A>()
    private let redoStack = UndoStack<A>()
    private let delegate: UndoDelegate
    private init() {fatalError()}
}

private extension Undo {
    private func push() {
        guard let undoItem = delegate.undoItem() as? A else {return}
        undoStack.push( undoItem )
    }
    
    private func pop() {
        guard let undoItem = undoStack.pop() else {return}
        if let redoItem = delegate.undoItem() as? A {
            redoStack.push( redoItem )
        }
        delegate.apply(undoItem: undoItem)
    }
}

class UndoStack<A> {
    private var items = [A]()
    var isEmpty: Bool {items.isEmpty}
    var count: Int {return items.count}
    func push(_ item: A) { items.append(item) }
    func pop() -> A? { items.isEmpty ? nil : items.removeLast() }
    func clear() { items = [A]() }
}
