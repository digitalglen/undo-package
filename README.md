# Undo

A simple undo/redo stack. 

Define the data you'll want to undo.

<pre><code>struct City: UndoItem {}
    let name: String
}
</code></pre>

Create the undo stack and your undo delegate methods
<pre><code>class Cities {
    var favorite: City?
    let undo = Undo<City>()
}    

extension Cities: UndoDelegate {
    func undoItem() -> City? {favorite}
    func apply(undoItem: City) {favorite = undoItem}
}
</code></pre>

Now, when you're about to modify the undoable data, save the old data first.
<pre><code>undo.save(favorite)
favorite = City(name: "Seattle")
</code></pre>

Enable UI for undo and redo as appropriate.
<pre><code>undoButton.isEnabled = undo.undoAvailable
redoButton.isEnabled = undo.redoAvailable
</code></pre>

And respond when the UI is invoked

<pre><code>@IBAction func tappedUndo() {undo.undo()}
@IBAction func tappedRedo() {undo.redo()}
</code></pre>
