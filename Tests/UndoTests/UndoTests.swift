import XCTest
@testable import Undo

final class UndoTests: XCTestCase {

    func testExample() {
        let cities = Cities()
        cities.add(city: "Rome")
        cities.add(city: "Paris")
        cities.add(city: "London")
        XCTAssertEqual(cities.undoCount, 3)
        XCTAssertEqual(cities.redoCount, 0)
        cities.undo()
        XCTAssertEqual(cities.undoCount, 2)
        XCTAssertEqual(cities.redoCount, 1)
        cities.undo()
        XCTAssertEqual(cities.undoCount, 1)
        XCTAssertEqual(cities.redoCount, 2)
        cities.redo()
        XCTAssertEqual(cities.undoCount, 2)
        XCTAssertEqual(cities.redoCount, 1)
        cities.clear()
        XCTAssertEqual(cities.undoCount, 0)
        XCTAssertEqual(cities.redoCount, 0)
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}

typealias City = String

class Cities {
    var favorite: City = "Madrid"
    private var _undo: Undo<City>?
    
    init() {
        _undo = Undo<City>(delegate: self)
    }
    
    func add(city: City) {
        _undo?.save()
        favorite = city
    }
    func undo() {_undo?.undo()}
    func redo() {_undo?.redo()}
    func clear() {_undo?.clear()}
    var undoCount: Int {_undo?.undoCount ?? 0}
    var redoCount: Int {_undo?.redoCount ?? 0}
}

extension Cities: UndoDelegate {
    func undoItem() -> Any? {return favorite}
    func apply(undoItem: Any) {favorite = undoItem as! City}
}
