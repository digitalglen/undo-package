import XCTest

import UndoTests

var tests = [XCTestCaseEntry]()
tests += UndoTests.allTests()
XCTMain(tests)
